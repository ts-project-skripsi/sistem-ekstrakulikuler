-- -------------------------------------------------------------
-- TablePlus 5.3.0(486)
--
-- https://tableplus.com/
--
-- Database: sistem_ekstrakulikuler
-- Generation Time: 2024-06-10 23:14:13.0710
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nama_anggota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `ekstrakulikuler`;
CREATE TABLE `ekstrakulikuler` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pembina_id` int DEFAULT NULL,
  `nama_ekstra` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `ekstrakulikuler_anggota`;
CREATE TABLE `ekstrakulikuler_anggota` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ekstrakulikuler_id` int DEFAULT NULL,
  `anggota_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `jadwal`;
CREATE TABLE `jadwal` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kegiatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `ekstrakulikuler_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `jadwal_absensi`;
CREATE TABLE `jadwal_absensi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `anggota_id` int DEFAULT NULL,
  `jadwal_id` int DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `lomba`;
CREATE TABLE `lomba` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kegiatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gambar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `nilai_anggota`;
CREATE TABLE `nilai_anggota` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ekstrakulikuler_id` int DEFAULT NULL,
  `dibuat_pada` datetime DEFAULT NULL,
  `nilai` decimal(15,2) DEFAULT NULL,
  `semester` varchar(50) DEFAULT NULL,
  `anggota_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `notifikasi`;
CREATE TABLE `notifikasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `color` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `pembina`;
CREATE TABLE `pembina` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `nama_pembina` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `pendaftaran`;
CREATE TABLE `pendaftaran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ekstrakulikuler_id` int DEFAULT NULL,
  `anggota_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` varchar(55) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman` (
  `id` int NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gambar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `profile` text,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `anggota` (`id`, `user_id`, `nama_anggota`, `tempat_lahir`, `tanggal_lahir`) VALUES
(14, 32, 'I GEDE ANDIKA ARY PRATAMA', 'Denpasar', '2002-03-15 00:00:00'),
(15, 33, 'Adinda Indradewi', 'Denpasar', '2024-06-07 00:00:00');

INSERT INTO `ekstrakulikuler` (`id`, `pembina_id`, `nama_ekstra`) VALUES
(1, 3, 'Sepak Bola'),
(2, 3, 'Basket'),
(3, 3, 'Renang'),
(4, 3, 'Pramuka'),
(5, 3, 'Web Desain'),
(6, 3, 'Robotik'),
(7, 3, 'Shooting');

INSERT INTO `ekstrakulikuler_anggota` (`id`, `ekstrakulikuler_id`, `anggota_id`) VALUES
(92, 2, 1),
(93, 1, 14),
(94, 5, 15),
(95, 1, 15),
(96, 1, 15);

INSERT INTO `jadwal` (`id`, `kegiatan`, `deskripsi`, `tanggal`, `ekstrakulikuler_id`) VALUES
(31, 'Line Up Kemenangan', 'deskripsi', '2024-06-06 00:00:00', 3),
(32, 'Pertandingan Persahabatan', 'anjay', '2024-06-08 00:00:00', 1),
(33, 'Menanam Mangrove', 'Ayo', '2024-06-09 00:00:00', 1),
(34, 'Menanam Mangrove', 'Ayo', '2024-06-09 00:00:00', 1),
(35, 'Menanam Mangrove', 'Ayo', '2024-06-09 00:00:00', 1),
(36, 'Menanam Mangrove', 'Ayo', '2024-06-09 00:00:00', 1);

INSERT INTO `jadwal_absensi` (`id`, `tanggal`, `anggota_id`, `jadwal_id`, `status`) VALUES
(33, '2024-06-08 02:43:37', 15, 32, 'tidak_hadir'),
(34, '2024-06-08 02:43:37', 14, 32, 'hadir');

INSERT INTO `lomba` (`id`, `kegiatan`, `deskripsi`, `gambar`, `tanggal`) VALUES
(31, 'ITCC 2023', 'lorem ipsum', '20240606022408.png', '2024-06-06 00:00:00'),
(34, 'FastTekno 2024', 'Lem biru dono kasino indro hayday coc clash royale mobile legend', '20240610143936.png', '2024-06-09 00:00:00'),
(36, 'FastTekno 2024', 'Lem biru dono kasino indro hayday coc clash royale mobile legend', '20240610144106.png', '2024-06-09 00:00:00');

INSERT INTO `nilai_anggota` (`id`, `ekstrakulikuler_id`, `dibuat_pada`, `nilai`, `semester`, `anggota_id`) VALUES
(93, 1, '2024-06-08 04:09:52', 88.00, 'ganjil', 15),
(94, 1, '2024-06-08 04:09:52', 89.00, 'ganjil', 14),
(95, 1, '2024-06-10 14:54:53', 55.00, 'genap', 15),
(96, 1, '2024-06-10 14:54:53', 55.00, 'genap', 14);

INSERT INTO `notifikasi` (`id`, `judul`, `deskripsi`, `tanggal`, `color`) VALUES
(33, 'Lomba Baru Ditambahkan', 'Lomba baru telah ditambahkan dengan nama FastTekno 2024', '2024-06-10 14:41:06', 'success'),
(34, 'Nilai Baru Ditambahkan', 'Nilai baru telah ditambahkan pada Sepak Bola semester genap', '2024-06-10 14:54:53', 'success');

INSERT INTO `pembina` (`id`, `user_id`, `nama_pembina`, `tempat_lahir`, `tanggal_lahir`) VALUES
(3, '10', 'Pak Jojo Kusumo', 'Dusuntegenan', '2024-03-30 00:00:00'),
(4, '21', 'Pak Jojo Jeje', 'Denpasar', '2024-03-21 00:00:00'),
(5, '27', 'I GEDE ANDIKA ARY PRATAMA', 'Denpasar', '2024-06-03 00:00:00'),
(6, '31', 'A.A. Putra Agung,S.Pd', 'Denpasar', '2024-06-08 00:00:00');

INSERT INTO `pendaftaran` (`id`, `ekstrakulikuler_id`, `anggota_id`, `status`, `tanggal`) VALUES
(6, 3, '1', 'diterima', '2024-06-04 13:27:44'),
(7, 2, '1', 'diterima', '2024-06-05 07:41:49'),
(8, 1, '14', 'diterima', '2024-06-07 07:05:03'),
(9, 5, '15', 'diterima', '2024-06-07 07:43:03'),
(10, 1, '15', 'diterima', '2024-06-07 07:45:25');

INSERT INTO `pengumuman` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(31, 'Batas Akhir Pendaftaran Lomba ITCC', 'Lorem ipsum donor sit amet ', '20240605163309.jpeg');

INSERT INTO `users` (`id`, `username`, `password`, `phone`, `role`, `alamat`, `profile`, `email`) VALUES
(1, 'admin', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'ADMIN', 'Jalan Raya Semer', 'noimage.jpeg', 'admin@gmail.com'),
(31, 'andikaary', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'PEMBINA', 'Alamatku', '20240607050256.png', 'andikaary99@gmail.com'),
(32, 'andikaaryyy', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Peliatan', 'noimage.jpeg', 'dekdepratama15@gmail.com'),
(33, 'adinda', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'dang ambian', '20240607074231.png', 'allmatilumord@gmail.com');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;