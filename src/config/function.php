<?php 

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    
    require 'PHPMailer/src/Exception.php';
    require 'PHPMailer/src/PHPMailer.php';
    require 'PHPMailer/src/SMTP.php';

    function sendEmail($email, $judul, $pesan) {
        $mail = new PHPMailer(true);
 
        try {                       
            $mail->SMTPDebug = 2;  
            $mail->isSMTP();
            $mail->Host       = 'smtp.gmail.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'firstaid142@gmail.com';
            $mail->Password   = 'qbzkogzprnrzyjnq';
            $mail->SMTPSecure = 'tls';
            $mail->Port       = 587;  
        
            $mail->setFrom('mail@gmail.com', 'Sistem Ekstrakulikuler');
            $mail->addAddress($email); 
            $mail->isHTML(true);
            $mail->Subject = $judul;    
            $mail->Body = $pesan;
            $mail->send();
        }catch (Exception $e) {
        }
    }
 

    // Function
    function validSession($role) {
        session_start();
        $redirect = 'login.php';
        if (isset($_SESSION['user']) && $_SESSION['user'] != NULL) {
            $session = $_SESSION['user'];
            if ($role[0] != 'LOGIN' && !in_array($session['role'], $role)) {
                session_destroy();
                header('location: '. $redirect);
                exit();
            } else {
                if ($role[0] == 'LOGIN') {
                    header('location: index.php');
                    exit(); 
                } 
            }
        } else {
            if ($role[0] != 'LOGIN') {
                header('location: '. $redirect);
                exit();
            }
        }
    }

    function createNotifikasi($conn, $param) 
    {
        $data = $conn->query("INSERT INTO notifikasi VALUES(NULL, '".$param[0]."', '".$param[1]."', '".$param[2]."', '".$param[3]."')");
    }

    function getDataTable($conn, $table)
    {
        $data = $conn->query("SELECT * FROM ".$table);
        return $data;
    }

    function getDataTableLimit($conn, $table, $limit)
    {
        $data = $conn->query("SELECT * FROM ".$table." LIMIT ".$limit);
        return $data;
    }

    function getDataTableWhereLike($conn, $table, $whereKey, $demitier, $whereValue)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$whereKey." ".$demitier." '".$whereValue."' ORDER BY ".$whereKey." ASC");
        return $data;
    }

    function getDataDetail($conn, $table, $id)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE id = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataDetailForeign($conn, $table, $id, $foreign)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataDetailForeign2($conn, $table, $id, $foreign)
    {
        $data = $conn->query("SELECT *, ".$table.".id as ".$table."_id FROM ".$table." WHERE ".$foreign." = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataDetailForeignAndWhere($conn, $table, $id, $foreign, $whereKey, $whereValue)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id." AND ".$whereKey." = '".$whereValue."' ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataDetailForeignOrder($conn, $table, $id, $foreign, $order, $ascdesc)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id." ORDER BY ".$table.".".$order." ".$ascdesc);
        return $data;
    }

    function getDataJoin($conn, $table, $join, $foreign)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinLimit($conn, $table, $join, $foreign, $limit = null, $id = null, $role = 'pembina')
    {
        $where_limit = '';
        if ($limit != null) {
            $where_limit = 'LIMIT '.$limit;
        }
        $where = '';
        if ($id != null) {
            $where = 'WHERE '.$role.'_id = '.$id;
        }
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." ".$where." ORDER BY ".$table.".id DESC ".$where_limit);
        return $data;
    }

    function getDataJoinWhere($conn, $table, $join, $foreign, $whereKey, $whereValue)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." WHERE ".$whereKey." = ".$whereValue." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMore($conn, $table, $join, $foreign, $join2, $foreign2)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." INNER JOIN ".$join2." ON ".$join2.".id = ".$table.".".$foreign2." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreLeft($conn, $table, $join, $foreign, $join2, $foreign2)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." LEFT JOIN ".$join2." ON ".$table.".id = ".$join2.".".$foreign2." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreLeftForeign($conn, $table, $join, $foreign, $join2, $foreign2, $where, $id)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." LEFT JOIN ".$join2." ON ".$join2.".id = ".$table.".".$foreign2." WHERE ".$where." = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreInnerForeign($conn, $table, $join, $foreign, $join2, $foreign2, $where, $id, $where2=null,$id2=null)
    {
        $setWhere = '';
        if ($where2 != null) {
            $setWhere = 'AND '.$where2."='".$id2."'";
        }
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." INNER JOIN ".$join2." ON ".$join2.".id = ".$table.".".$foreign2." WHERE ".$where." = ".$id." ".$setWhere." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreInnerForeign2($conn, $table, $join, $foreign, $join2, $foreign2, $where, $id, $where2=null,$id2=null)
    {
        $setWhere = '';
        if ($where2 != null) {
            $setWhere = 'AND '.$where2."='".$id2."'";
        }
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." INNER JOIN ".$join2." ON ".$join2.".id = ".$foreign2." WHERE ".$where." = ".$id." ".$setWhere." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreDetail($conn, $table, $join, $foreign, $join2, $foreign2, $id)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." INNER JOIN ".$join2." ON ".$join2.".id = ".$table.".".$foreign2." WHERE ".$table.".id = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinDetail($conn, $table, $join, $foreign, $id)
    {
        // var_dump("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." WHERE ".$table."_id = ".$id);die();
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." WHERE ".$table.".id = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataFile($conn, $table, $foreign, $id)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id);
        return $data;
    }

    function checkUsername($conn, $username, $id = 0) {
        $andWhere = '';
        if ($id != 0) {
            $andWhere = ' AND id != '.$id;
        }
        $data = $conn->query("SELECT * FROM users WHERE username = '".$username."' ".$andWhere);
        $status = $data->num_rows > 0 ? false : true;
        return $status;
    }

    function getDataToArray($array, $key = null) {
        $result = [];
        while ($data = $array->fetch_assoc()) {
            if ($key != null) {
                $result[$data[$key]][] = $data;
            } else {
                $result[] = $data;
            }
        }
        return $result;
    }

    function getPosisi() {
        $posisi = [
            'penyerang' => 'Pemain depan', 
            'gelandang' => 'Pemain tengah', 
            'bek' => 'Pemain bertahan', 
            // 'kiper' => 'Kiper'
        ];
        return $posisi;
    }

    function getTypeKriteria() {
        $type = [
            'benefit' => 'Benefit', 
            'cost' => 'Cost', 
        ];
        return $type;
    }

    function getStatusPertandingan() {
        $status = [
            'belum_bertanding' => 'Belum Bertanding', 
            'menang' => 'Menang', 
            'seri' => 'Seri',
            'kalah' => 'Kalah', 
        ];
        return $status;
    }

    function getTypeStatistik() {
        $type = [
            'assist' => 'Assist', 
            'goal' => 'Goal', 
            'start' => 'Start', 
            'yellow_card' => 'Yellow Card', 
            'red_card' => 'Red Card', 
        ];
        return $type;
    }

    function normalisasi($type, $nilai, $min_max) {
        if ($type=='benefit'){
            $result=$nilai/$min_max['max'];
        }else{
            $result=$min_max['min']/$nilai;
        }
        return $result;
    }

    function max_min_grade($latihan_pemains) {
        $data = [];
        foreach ($latihan_pemains as $key => $value) {
            $data[$value['posisi']][] = $value['nilai'];
        }
        $min_max = [];
        foreach ($data as $key => $value) {
            $min_max[$key]['min'] = min($value);
            $min_max[$key]['max'] = max($value);
        }

        return $min_max;
    }

?>