<?php 
    include "../config/connection.php";
    include "../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    if (isset($_GET['id'])) {
        try {
            $conn->autocommit(FALSE);
            $conn->query('UPDATE '.$_GET['tabel'].' SET status="'.$_GET['status'].'" WHERE id='.$_GET['id']);
            if ($_GET['status'] == 'diterima') {
                $pendaftaran = getDataDetail($conn, 'pendaftaran', $_GET['id'])->fetch_assoc();
                $conn->query("INSERT INTO ekstrakulikuler_anggota VALUES(NULL, '".htmlspecialchars($pendaftaran['ekstrakulikuler_id'])."', '".htmlspecialchars($pendaftaran['anggota_id'])."')");
                $ekstrakulikuler = getDataDetail($conn, 'ekstrakulikuler', $pendaftaran['ekstrakulikuler_id'])->fetch_assoc();
                $anggota = getDataDetail($conn, 'anggota', $pendaftaran['anggota_id'])->fetch_assoc();
                $user = getDataDetail($conn, 'users', $anggota['user_id'])->fetch_assoc();
                sendEmail($user['email'], 'Pendaftaran Disetujui', 'Pendaftaran Ekstrakulikuler '.$ekstrakulikuler['nama_ekstra'].' telah disetujui');
            }
            $updated = true;
            $conn->commit();
        } catch (Exception $e) {
            $updated = false;
            $conn->rollback();
            $conn->close();
        }
        if ($updated) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah status';
        } else {
            $response['error']   = true;
            $response['icon']    = 'error';
            $response['message'] = 'Gagal mengubah status';
        }
        
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'ID tidak ditemukan';
    }
    $_SESSION['alert_'.$_GET['redirect']] = $response;
    
    header('location: ../view/'.$_GET['redirect'].'/index.php');
?>