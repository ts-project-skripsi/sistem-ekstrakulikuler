<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);

            
        try {
            $conn->autocommit(FALSE);
            $conn->query("UPDATE ekstrakulikuler SET nama_ekstra='".htmlspecialchars($nama)."', pembina_id=".$pembina." WHERE id=".$_GET['id']);
            $update = true;
            $conn->commit();
        } catch (Exception $e) {
            $update = false;
            $conn->rollback();
            $conn->close();
        }
        if ($update) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal mengubah data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_ekstrakulikuler'] = $response;
    
    header('location: ../../view/ekstrakulikuler/index.php');
    exit(); 
?>