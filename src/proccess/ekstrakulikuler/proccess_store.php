<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $conn->query("INSERT INTO ekstrakulikuler VALUES(NULL,".$pembina.", '".htmlspecialchars($nama)."')");
            $insert = true;
            createNotifikasi($conn,['Ekstrakulikuler Baru Ditambahkan', 'Ekstrakulikuler baru telah ditambahkan dengan nama '.$nama, date('Y-m-d H:i:s'), 'success']);
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_ekstrakulikuler'] = $response;
    
    header('location: ../../view/ekstrakulikuler/index.php');
    exit(); 
?>