<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        $new_file = 'noimage.jpeg';
        if (isset($_FILES)) {
            $ekstensi      = ['png','jpg','jpeg','gif', 'pdf'];
            $nama_file     = explode('.', $_FILES['gambar']['name']);
            $ekstensi_file = strtolower(end($nama_file));
            $file_tmp      = $_FILES['gambar']['tmp_name'];
            if (in_array($ekstensi_file, $ekstensi)) {
                $new_file  = date('YmdHis').'.'.$ekstensi_file;
                move_uploaded_file($file_tmp, '../../../upload/'.$new_file);
            }
        }

        try {
            $conn->autocommit(FALSE);
            $user = $conn->query("INSERT INTO lomba VALUES(NULL,'".htmlspecialchars($nama)."', '".htmlspecialchars($deskripsi)."', '".$new_file."', '".htmlspecialchars($tanggal)."')");
            $user_id = $conn->insert_id;
            $insert = true;
            createNotifikasi($conn,['Lomba Baru Ditambahkan', 'Lomba baru telah ditambahkan dengan nama '.$nama, date('Y-m-d H:i:s'), 'success']);
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_lomba'] = $response;
    
    header('location: ../../view/lomba/index.php');
    exit(); 
?>