<?php 
    include "../config/connection.php";
    session_start();
    extract($_POST);
    $check_user = $conn->query('SELECT * FROM users WHERE username = "'.htmlspecialchars($username).'"')->fetch_assoc();
    if ($check_user != NULL) {
        $valid_user = $conn->query('SELECT * FROM users WHERE username = "'.$username.'" AND password = "'.sha1(htmlspecialchars($password)).'"')->fetch_assoc();
        if ($valid_user != NULL) {
            if ($valid_user['role'] != '') {
                session_start();
                $_SESSION['user']    = $valid_user;
                $response['error']   = false;
                $response['message'] = 'Login berhasil';
                $redirect = '../view/index.php';
            } else {
                $response['error']   = true;
                $response['message'] = 'Anda tidak memiliki Akses';
                $redirect = '../view/login.php';
            }
        } else {
            $response['error']   = true;
            $response['message'] = 'Password yang dimasukan salah';
            $redirect = '../view/login.php';
        }
    } else {
        $response['error']   = true;
        $response['message'] = 'Username tidak tersedia';
        $redirect = '../view/login.php';
    }

    $_SESSION['alert_login'] = $response;
    header('location: '.$redirect);
?>