<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $conn->query("INSERT INTO pendaftaran VALUES(NULL, '".htmlspecialchars($ekstrakulikuler)."', '".htmlspecialchars($_GET['id'])."', 'pending', '".date('Y-m-d H:i:s')."')");
            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pendaftaran'] = $response;
    
    header('location: ../../view/pendaftaran/index.php');
    exit(); 
?>