<?php
    ob_start();
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        $data = getDataJoinMoreInnerForeign2($conn, 'ekstrakulikuler_anggota', 'anggota', 'anggota_id', 'users', 'anggota.user_id', 'ekstrakulikuler_anggota.ekstrakulikuler_id', $ekstrakulikuler);
        $anggota = getDataToArray($data);


        try {
            $conn->autocommit(FALSE);
            $user = $conn->query("UPDATE jadwal SET kegiatan='".htmlspecialchars($nama)."', deskripsi='".htmlspecialchars($deskripsi)."', tanggal='".htmlspecialchars($tanggal." 00:00:00")."', ekstrakulikuler_id='".htmlspecialchars($ekstrakulikuler)."' WHERE id = ".$_GET['id']);
            $user_id = $conn->insert_id;
            $insert = true;
            createNotifikasi($conn,['Jadwal Diubah', 'Jadwal telah diubah dengan nama '.$nama, date('Y-m-d H:i:s'), 'success']);
            $conn->commit();

            foreach ($anggota as $key => $value) {
                sendEmail($value['email'], 'Jadwal telah diubah - '.$nama, $deskripsi);
            }

        } catch (Exception $e) {
            $insert = false;
            $conn->rollback();
            $conn->close();
            var_dump($e);die();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal mengubah data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_jadwal'] = $response;
    header('location: ../../view/jadwal/index.php');
    exit(); 
    ob_end_flush();

?>