<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);

        try {
            $data_absen = getDataDetailForeign($conn, 'jadwal_absensi', $_GET['id'], 'jadwal_id');
            if ($data_absen->num_rows > 0) {
                $conn->autocommit(FALSE);
                $deleted = $conn->query("DELETE FROM jadwal_absensi WHERE jadwal_id = ". $_GET['id']);
                $conn->commit();
            } 
            foreach ($absen as $key => $value) {
                $status = $value == 1 ? 'hadir' : 'tidak_hadir';
                $conn->autocommit(FALSE);
                $user = $conn->query("INSERT INTO jadwal_absensi VALUES(NULL,'".date('Y-m-d H:i:s')."', '".htmlspecialchars($key)."', '".$_GET['id']."', '".htmlspecialchars($status)."')");
                $user_id = $conn->insert_id;
                $insert = true;
                $conn->commit();
            }
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan absen';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_jadwal'] = $response;
    
    header('location: ../../view/jadwal/index.php');
    exit(); 
?>