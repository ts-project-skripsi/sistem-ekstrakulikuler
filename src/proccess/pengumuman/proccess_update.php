<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    
    if (isset($_POST)) {
        extract($_POST);
        $new_file = null;
        if (isset($_FILES)) {
            $ekstensi      = ['png','jpg','jpeg','gif', 'pdf'];
            $nama_file     = explode('.', $_FILES['gambar']['name']);
            $ekstensi_file = strtolower(end($nama_file));
            $file_tmp      = $_FILES['gambar']['tmp_name'];
            if (in_array($ekstensi_file, $ekstensi)) {
                $new_file  = date('YmdHis').'.'.$ekstensi_file;
                move_uploaded_file($file_tmp, '../../../upload/'.$new_file);
            }
        }

        $cansave = true;
        

        if ($cansave) {
            $gambar_change = '';
            if ($new_file != null) {
                $gambar_change = ", gambar='".$new_file."'";
            }
            
            try {
                $conn->autocommit(FALSE);
                $conn->query("UPDATE pengumuman SET judul='".htmlspecialchars($nama)."', deskripsi='".htmlspecialchars($deskripsi)."'".$gambar_change." WHERE id=".$_GET['id']);
                $update = true;
                $conn->commit();
            } catch (Exception $e) {
                var_dump($e);die();
                $update = false;
                $conn->rollback();
                $conn->close();
            }
            if ($update) {
                $response['error']   = false;
                $response['icon']    = 'success';
                $response['message'] = 'Berhasil mengubah data';
            } else {
                $response['error']   = true;
                $response['icon']    = 'danger';
                $response['message'] = 'Gagal mengubah data';
            }
        } else {
            $response['error']   = true;
            $response['icon']    = 'warning';
            $response['message'] = 'Password dan Konfirm Password harus sama';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pengumuman'] = $response;
    if (isset($_GET['profile']) && $_GET['profile']) {
        $redirect = 'index.php';
    } else {
        $redirect = 'pengumuman/index.php';
    }
    header('location: ../../view/'.$redirect);
    exit(); 
?>