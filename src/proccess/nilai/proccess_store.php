<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);

        try {
            $data_absen = getDataDetailForeignAndWhere($conn, 'nilai_anggota', $ekstrakulikuler, 'ekstrakulikuler_id', 'nilai_anggota.semester', $semester);
            $conn->autocommit(FALSE);
            $ekstra = getDataDetail($conn, 'ekstrakulikuler', $ekstrakulikuler)->fetch_assoc();
            if ($data_absen->num_rows > 0) {
                $deleted = $conn->query("DELETE FROM nilai_anggota WHERE ekstrakulikuler_id = ". $ekstrakulikuler. " AND nilai_anggota.semester='".$semester."'");
            } 

            createNotifikasi($conn,['Nilai Baru Ditambahkan', 'Nilai baru telah ditambahkan pada '.$ekstra['nama_ekstra'].' semester '.$semester, date('Y-m-d H:i:s'), 'success']);
            $conn->commit();
            foreach ($nilai as $key => $value) {
                $conn->autocommit(FALSE);
                $user = $conn->query("INSERT INTO nilai_anggota VALUES(NULL, '".$ekstrakulikuler."','".date('Y-m-d H:i:s')."', '".htmlspecialchars($value)."', '".$semester."', '".$key."')");
                $user_id = $conn->insert_id;
                $insert = true;
                $conn->commit();
            }
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_nilai'] = $response;
    
    header('location: ../../view/nilai/index.php');
    exit(); 
?>