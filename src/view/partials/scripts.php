
<script src="../../../assets/js/vendors.min.js"></script>
<script src="../../../assets/js/pages/chat-popup.js"></script>
<script src="../../../assets/icons/feather-icons/feather.min.js"></script>
<script src="../../../assets/vendor_components/chart.js-master/Chart.min.js"></script>


<!-- <script src="../../../assets/vendor_components/apexcharts-bundle/dist/apexcharts.js"></script> -->
<script src="../../../assets/vendor_components/moment/min/moment.min.js"></script>
<script src="../../../assets/vendor_components/fullcalendar/fullcalendar.js"></script>

<!-- EduAdmin App -->
<script src="../../../assets/js/template.js"></script>
<script src="../../../assets/js/pages/dashboard.js"></script>
<script src="../../../assets/js/pages/calendar.js"></script>
<!-- <script src="../../../assets/js/pages/widget-charts2.js"></script> -->
<script src="../../../assets/vendor_components/datatable/datatables.min.js"></script>
<script src="../../../assets/js/pages/data-table.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    function deleteData(id, tabel, redirect, user_id = 0) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Yakin ingin menghapus data",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yakin',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                document.location.href = '../../proccess/proccess_delete.php?id='+id+'&tabel='+tabel+'&redirect='+redirect+'&user_id='+user_id;
            }
        })
    }

    function updateStatus(id, tabel, redirect, user_id = 0, status = 'pending') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Yakin ingin mengubah status",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yakin',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                document.location.href = '../../proccess/proccess_update_status.php?id='+id+'&tabel='+tabel+'&redirect='+redirect+'&user_id='+user_id+'&status='+status;
            }
        })
    }

    $(document).ready(function () {
        $('.fancybox_file').fancybox();

        $('.btn-delete-data').on('click', function () {
            deleteData($(this).data('id'), $(this).data('tabel'), $(this).data('redirect'), $(this).data('user_id'));
        });

        $('.btn-update-data').on('click', function () {
            updateStatus($(this).data('id'), $(this).data('tabel'), $(this).data('redirect'), $(this).data('user_id'), $(this).data('status'));
        });
    });
</script>