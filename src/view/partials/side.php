
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">	
	  	<div class="multinav">
		  <div class="multinav-scroll" style="height: 100%;">	
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">	
				<li class="header">Dashboard</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>index.php">
					<i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
					<span>Dashboard</span>
				  </a>
				</li>
				<li class="header">Pengguna</li>
				<?php 
					if ($_SESSION['user']['role'] != 'USER') { 
				?>
					<li>
					  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>pembina/index.php">
						<i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
						<span>Pembina</span>
					  </a>
					</li>
				<?php } ?>
				<?php if ($_SESSION['user']['role'] != 'PEMBINA') { ?>
					<li>
					<a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>anggota/index.php">
						<i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
						<span>Anggota</span>
					</a>
					</li>
				<?php } ?>
				<li class="header">Ekstrakulikuler</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>ekstrakulikuler/index.php">
					<i class="icon-Sneakers"><span class="path1"></span><span class="path2"></span></i>
					<span>Ekstrakulikuler</span>
				  </a>
				</li>
				<li>
					<a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>pendaftaran/index.php">
					<i class="icon-Clipboard-check"><span class="path1"></span><span class="path2"></span></i>
					<span>Pendaftaran</span>
					</a>
				</li>
				<li class="header">Jadwal & Nilai</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>nilai/index.php">
					<i class="icon-Equalizer"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
					<span>Nilai</span>
				  </a>
				</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>jadwal/index.php">
					<i class="icon-Timer"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
					<span>Jadwal</span>
				  </a>
				</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>lomba/index.php">
					<i class="icon-Crown"><span class="path1"></span><span class="path2"></span></i>
					<span>Lomba</span>
				  </a>
				</li>	 
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>pengumuman/index.php">
					<i class="icon-Notifications"><span class="path1"></span><span class="path2"></span></i>
					<span>Pengumuman</span>
				  </a>
				</li>    
			  </ul>
		  </div>
		</div>
    </section>
  </aside>