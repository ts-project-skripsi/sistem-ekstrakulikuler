<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    $breadcumb = 'lomba';
    if ($_SESSION['user']['role'] != 'USER') {
        $data = getDataJoinMoreInnerForeign($conn, 'nilai_anggota', 'ekstrakulikuler', 'ekstrakulikuler_id', 'anggota', 'anggota_id', 'nilai_anggota.ekstrakulikuler_id', $_GET['id']);
    } else {
		$anggota = getDataDetailForeign($conn, 'anggota', $_SESSION['user']['id'], 'user_id')->fetch_assoc();
        $data = getDataJoinMoreInnerForeign($conn, 'nilai_anggota', 'ekstrakulikuler', 'ekstrakulikuler_id', 'anggota', 'anggota_id', 'nilai_anggota.ekstrakulikuler_id', $_GET['id'], 'nilai_anggota.anggota_id', $anggota['id']);
    }
    
    $nilai = getDataToArray($data, 'semester');
    $data_ekstra = getDataDetail($conn, 'ekstrakulikuler', $_GET['id']);
    $ekstrakulikuler = $data_ekstra->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition ligth-skin sidebar-mini theme-primary fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Nilai <?= $ekstrakulikuler['nama_ekstra'] ?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Nilai</li>
								<li class="breadcrumb-item active" aria-current="page">Detail Nilai</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-6 col-6">
                        <div class="box">
                            <div class="box-header with-border">
                                <div class="col-12 d-flex justify-content-between">
                                    <h3 class="box-title">List Nilai Semester Ganjil</h3>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                <table id="example123" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Anggota</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($nilai['ganjil']) && count($nilai['ganjil'])) { ?>
                                            <?php
                                                foreach ($nilai['ganjil'] as $key => $value) {
                                            ?>
                                                <tr>
                                                    <td><?= $value['nama_anggota'] ?></td>
                                                    <td><?= round($value['nilai']) ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="2" class="text-center">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Anggota</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
				<div class="col-xl-6 col-6">
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">List Nilai Semester Genap</h3>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                <table id="example123" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Anggota</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($nilai['genap']) && count($nilai['genap'])) { ?>
                                            <?php
                                                foreach ($nilai['genap'] as $key => $value) {
                                            ?>
                                                <tr>
                                                    <td><?= $value['nama_anggota'] ?></td>
                                                    <td><?= round($value['nilai']) ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="2" class="text-center">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Anggota</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Ekstrakulikuler</a>. SMK TI BALI GLOBAL DENPASAR.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
