<?php
    include "../config/connection.php";
    include "../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    $breadcumb = 'dashboard';

	if ($_SESSION['user']['role'] == 'ADMIN') {
		$data = getDataJoinLimit($conn, 'ekstrakulikuler', 'pembina', 'pembina_id', 4);
	} else if ($_SESSION['user']['role'] == 'PEMBINA') {
		$pembina = getDataDetailForeign($conn, 'pembina', $_SESSION['user']['id'], 'user_id')->fetch_assoc();
		$data = getDataJoinLimit($conn, 'ekstrakulikuler', 'pembina', 'pembina_id', 4, $pembina['id']);
	} else {
		$anggota = getDataDetailForeign($conn, 'anggota', $_SESSION['user']['id'], 'user_id')->fetch_assoc();
		$data = getDataJoinMoreInnerForeign2($conn, 'ekstrakulikuler_anggota', 'ekstrakulikuler', 'ekstrakulikuler_id', 'pembina', 'ekstrakulikuler.pembina_id', 'ekstrakulikuler_anggota.anggota_id', $anggota['id']);
	}
    $ekstrakulikuler = getDataToArray($data);

	$data_pengumuman = getDataTableLimit($conn, 'pengumuman', 4);
	$pengumuman = getDataToArray($data_pengumuman);

	$data_lomba = getDataTableLimit($conn, 'lomba', 4);
	$lomba = getDataToArray($data_lomba);

	$data_notifikasi = getDataTableLimit($conn, 'notifikasi', 4);
	$notifikasi = getDataToArray($data_notifikasi);

	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('partials/links.php'); ?>
     
  </head>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('partials/nav.php'); ?>
	<?php require('partials/side.php'); ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-12">
					<div class="box bg-gradient-primary overflow-hidden pull-up">
						<div class="box-body pe-0 ps-lg-50 ps-15 py-0">							
							<div class="row align-items-center">
								<div class="col-12 col-lg-8">
									<h1 class="fs-40 text-white">Selamat Datang <?= $_SESSION['user']['username'] ?>!</h1>
									<p class="text-white mb-0 fs-20">
										di Sistem Ekstrakulikuler SMK TI BALI GLOBAL DENPASAR
									</p>
								</div>
								<div class="col-12 col-lg-4"><img src="../../assets/images/svg-icon/color-svg/custom-15.svg" alt=""></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">														
					<div class="box no-shadow mb-0 bg-transparent">
						<div class="box-header no-border px-0">
							<h4 class="box-title">Ekstrakulikuler</h4>	
							<ul class="box-controls pull-right d-md-flex d-none">
							  <li>
								<a href="ekstrakulikuler/index.php" type="button" class="btn btn-primary-light px-10">View All</a>
							  </li>
							</ul>
						</div>
					</div>
				</div>

				<?php if (count($ekstrakulikuler) > 0) { ?>
					<?php
						$color = ['primary', 'success', 'warning', 'danger'];
						foreach ($ekstrakulikuler as $key => $value) {
					?>
							<div class="col-xl-3 col-md-6 col-12">
								<div class="box pull-up">
									<div class="box-body">	
										<div class="bg-<?= $color[$key] ?> rounded">
											<h5 class="text-white text-center p-10"><?= $value['nama_ekstra'] ?></h5>
										</div>
										<p class="mb-0 fs-18"><b>Pembina</b></p>
										<p class="mb-0 fs-18"><?= $value['nama_pembina'] ?></p>							
									</div>					
								</div>
							</div>
					<?php 
						} 
					?>
				<?php } ?>
			</div>
			<div class="row">
				<div class="col-xl-6 col-6">
					<div class="box">
						<div class="box-header with-border">
							<h4 class="box-title">Pengumuman</h4>
						</div>
						<div class="box-body p-0">
						  <div class="media-list media-list-hover">

						  	<?php if (count($pengumuman) > 0) { ?>
								<?php
									foreach ($pengumuman as $key => $value) {
								?>
									<div class="media bar-0">
									<span class="avatar avatar-lg rounded"><img src="../../upload/<?= $value['gambar'] ?>" style="width: 100px;" alt=""></span>
									<div class="media-body font-weight-500">
										<p class="d-flex align-items-center justify-content-between">
										<a class="hover-success" href="#"><strong><?= $value['judul'] ?></strong></a>
										<span class="text-fade font-weight-500 font-size-12"></span>
										</p>
										<p class="text-fade"><?= strlen($value['deskripsi']) > 50 ? substr($value['deskripsi'], 0, 50).'...' : $value['deskripsi'] ?></p>
									</div>
									</div>
								<?php } ?>
							<?php } else { ?>
								<div class="media bar-0">
									<div class="media-body font-weight-500">
										<p class="text-fade">Tidak ada data</p>
									</div>
								</div>
							<?php } ?>

						  </div>
						</div>
						<div class="box-footer text-center p-10">
							<a href="pengumuman/index.php" class="btn btn-block btn-primary-light p-5">View all</a>
						</div>
					</div>
				</div>		
				<div class="col-xl-6 col-6">
					<div class="box">
						<div class="box-header with-border">
							<h4 class="box-title">Perlombaan</h4>
						</div>
						<div class="box-body p-0">
						  <div class="media-list media-list-hover">
							
						  <?php if (count($lomba) > 0) { ?>
								<?php
									foreach ($lomba as $key => $value) {
								?>
									<div class="media bar-0">
										<span class="avatar avatar-lg rounded"><img src="../../upload/<?= $value['gambar'] ?>" style="width: 100px;" alt=""></span>
										<div class="media-body font-weight-500">
											<p class="d-flex align-items-center justify-content-between">
												<a class="hover-success" href="#"><strong><?= $value['kegiatan'] ?></strong></a>
												<span class="text-fade font-weight-500 font-size-12"><?= date('d M Y', strtotime($value['tanggal'])) ?></span>
											</p>
											<p class="text-fade"><?= strlen($value['deskripsi']) > 50 ? substr($value['deskripsi'], 0, 50).'...' : $value['deskripsi'] ?></p>
										</div>
									</div>
								<?php } ?>
							<?php } else { ?>
								<div class="media bar-0">
									<div class="media-body font-weight-500">
										<p class="text-fade">Tidak ada data</p>
									</div>
								</div>
							<?php } ?>

						  </div>
						</div>
						<div class="box-footer text-center p-10">
							<a href="lomba/index.php" class="btn btn-block btn-primary-light p-5">View all</a>
						</div>
					</div>
				</div>		

				<?php if ($_SESSION['user']['role'] == 'USER') { ?>
					<div class="col-xl-12 col-12">
						<div class="box">
							<div class="box-header with-border">
								<h4 class="box-title">Jadwalku</h4>
							</div>
							<div class="box-body p-0">
							<div class="media-list media-list-hover">
								
								<?php if (count($lomba) > 0) { ?>
									<?php
										foreach ($lomba as $key => $value) {
									?>
										<div class="media bar-0">
											<span class="avatar avatar-lg rounded"><img src="../../upload/<?= $value['gambar'] ?>" style="width: 100px;" alt=""></span>
											<div class="media-body font-weight-500">
												<p class="d-flex align-items-center justify-content-between">
													<a class="hover-success" href="#"><strong><?= $value['kegiatan'] ?></strong></a>
													<span class="text-fade font-weight-500 font-size-12"><?= date('d M Y', strtotime($value['tanggal'])) ?></span>
												</p>
												<p class="text-fade"><?= strlen($value['deskripsi']) > 50 ? substr($value['deskripsi'], 0, 50).'...' : $value['deskripsi'] ?></p>
											</div>
										</div>
									<?php } ?>
								<?php } else { ?>
									<div class="media bar-0">
										<div class="media-body font-weight-500">
											<p class="text-fade">Tidak ada data</p>
										</div>
									</div>
								<?php } ?>

							</div>
							</div>
							<div class="box-footer text-center p-10">
								<a href="lomba/index.php" class="btn btn-block btn-primary-light p-5">View all</a>
							</div>
						</div>
					</div>	

				<?php } ?>
				


				<div class="col-xl-12 col-12">
					<div class="box">
						<div class="box-header with-border">
							<h4 class="box-title">Notifikasi</h4>
						</div>
						<div class="box-body p-0">
						<div class="media-list media-list-hover">
							
							<?php if (count($notifikasi) > 0) { ?>
								<?php
									foreach ($notifikasi as $key => $value) {
								?>
									<div class="media bar-0">
							 			<span class="avatar avatar-lg bg-<?= $value['color'] ?>-light rounded"><i class="fa fa-bullhorn"></i></span>
										<div class="media-body font-weight-500">
											<p class="d-flex align-items-center justify-content-between">
												<a class="hover-success" href="#"><strong><?= $value['judul'] ?></strong></a>
												<span class="text-fade font-weight-500 font-size-12"><?= date('d M Y', strtotime($value['tanggal'])) ?></span>
											</p>
											<p class="text-fade"><?= strlen($value['deskripsi']) > 100 ? substr($value['deskripsi'], 0, 100).'...' : $value['deskripsi'] ?></p>
										</div>
									</div>
								<?php } ?>
							<?php } else { ?>
								<div class="media bar-0">
									<div class="media-body font-weight-500">
										<p class="text-fade">Tidak ada data</p>
									</div>
								</div>
							<?php } ?>

						</div>
						</div>
						<div class="box-footer text-center p-10">
							<a href="notifikasi/index.php" class="btn btn-block btn-primary-light p-5">View all</a>
						</div>
					</div>
				</div>	
			</div>			
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Ekstrakulikuler</a>. SMK TI BALI GLOBAL DENPASAR.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('partials/scripts.php'); ?>
	<script>
		$(document).ready(function () {
			var data_chart = <?= json_encode($chart_data_pertandingan) ?>;
			var label_chart = <?= json_encode($chart_label_pertandingan) ?>;
			var data_statistik = <?= json_encode($chart_data_statistik) ?>;
			var label_statistik = <?= json_encode($chart_label_statistik) ?>;
			/* large line chart */
			var chLine = document.getElementById("chLine");
			var chartData = {
			labels: label_chart,
			datasets: [
			{
				data: data_chart,
				// backgroundColor: colors[3],
				borderColor: '#ff562f',
				borderWidth: 4,
				pointBackgroundColor: '#ff562f'
			}]
			};
			if (chLine) {
			new Chart(chLine, {
			type: 'line',
			data: chartData,
			options: {
				scales: {
				yAxes: [{
					ticks: {
					beginAtZero: false
					}
				}]
				},
				legend: {
				display: false
				},
				responsive: true
			}
			});
			}


			var ctx3 = document.getElementById("radar-chart2").getContext("2d");
		var data3 = {
			labels: label_statistik,
			datasets: [
				{
					label: "Statistik Pemain",
					backgroundColor: "#ff562f82",
					borderColor: "#ff562f",
					pointBackgroundColor: "#ff562f",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "#ff562f",
					data: data_statistik
				}
			]
		};
		var radarChart = new Chart(ctx3, {
			type: "radar",
			data: data3,
			options: {
					scale: {
						ticks: {
							beginAtZero: true,
							fontFamily: "Nunito Sans",
							
						},
						gridLines: {
							color: "rgba(135,135,135,0)",
						},
						pointLabels:{
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
						},
					},
					
					animation: {
						duration:	3000
					},
					responsive: true,
					legend: {
							labels: {
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
							}
						},
						elements: {
							arc: {
								borderWidth: 0
							}
						},
						tooltip: {
						backgroundColor:'rgba(33,33,33,1)',
						cornerRadius:0,
						footerFontFamily:"'Nunito Sans'"
					}
			}
		});
		})
	</script>
</body>
</html>
