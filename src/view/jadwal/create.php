<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    $breadcumb = 'jadwal';
    if ($_SESSION['user']['role'] == 'ADMIN') {
        $data = getDataTable($conn,'ekstrakulikuler');
    } else {
		$pembina = getDataDetailForeign($conn, 'pembina', $_SESSION['user']['id'], 'user_id')->fetch_assoc();
		$data = getDataJoinLimit($conn, 'ekstrakulikuler', 'pembina', 'pembina_id', null, $pembina['id']);
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Jadwal</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Jadwal</li>
								<li class="breadcrumb-item active" aria-current="page">Tambah Jadwal</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">Tambah Jadwal</h3>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="POST" action="../../proccess/jadwal/proccess_store.php" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Kegiatan</label>
                                                <input type="text" class="form-control" placeholder="Kegiatan" name="nama" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Tanggal</label>
                                                <input type="date" class="form-control" placeholder="Tanggal" name="tanggal">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Ekstrakulikuler</label>
                                                <select name="ekstrakulikuler" class="form-control" required>
                                                    <option value="">Pilih Ekstrakulikuler</option>
                                                    <?php foreach ($data as $key => $value) { ?>
                                                        <option value="<?= $value['id'] ?>"><?= $value['nama_ekstra'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea class="form-control" placeholder="Deskripsi" name="deskripsi" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <a href="index.php" type="button" class="btn btn-rounded btn-warning btn-outline mr-1">
                                        <i class="ti-arrow-left"></i> Kembali
                                    </a>
                                    <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                                        <i class="ti-save-alt"></i> Simpan
                                    </button>
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Ekstrakulikuler</a>. SMK TI BALI GLOBAL DENPASAR.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
