<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PEMBINA', 'USER']);
    $breadcumb = 'jadwalabsen';
    $data = getDataJoinMoreInnerForeign($conn, 'ekstrakulikuler_anggota', 'ekstrakulikuler', 'ekstrakulikuler_id', 'anggota', 'anggota_id', 'ekstrakulikuler_anggota.ekstrakulikuler_id', $_GET['ekstra_id']);
    if ($_SESSION['user']['role'] == 'USER') {
		$anggota = getDataDetailForeign($conn, 'anggota', $_SESSION['user']['id'], 'user_id')->fetch_assoc();
        $data = getDataJoinMoreInnerForeign2($conn, 'ekstrakulikuler_anggota', 'ekstrakulikuler', 'ekstrakulikuler_id', 'anggota', 'anggota_id', 'ekstrakulikuler_anggota.ekstrakulikuler_id', $_GET['ekstra_id'], 'ekstrakulikuler_anggota.anggota_id', $anggota['id']);
    }
    $data_2 = getDataJoinMoreInnerForeign($conn, 'ekstrakulikuler_anggota', 'ekstrakulikuler', 'ekstrakulikuler_id', 'anggota', 'anggota_id', 'ekstrakulikuler_anggota.ekstrakulikuler_id', $_GET['ekstra_id']);
    $data_absen = getDataDetailForeign($conn, 'jadwal_absensi', $_GET['id'], 'jadwal_id');
    $absen = getDataToArray($data_absen, 'anggota_id');
    $ekstra = getDataDetail($conn, 'ekstrakulikuler', $_GET['ekstra_id'])->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Anggota Ekstrakulikuler <?= $ekstra['nama_ekstra'] ?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Ekstrakulikuler</li>
								<li class="breadcrumb-item active" aria-current="page">Anggota</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
        <form action="../../proccess/jadwal/proccess_absen.php?id=<?= $_GET['id'] ?>" method="post">
            <section class="content">
                <div class="row">
                    <div class="col-xl-12 col-12">

                            <?php if ($data->num_rows > 0 && $_SESSION['user']['role'] == 'USER') { ?>
                                <?php
                                    $isHadir = false;
                                    $checked = '';
                                    while ($value = $data_2->fetch_assoc()) {
                                        if ($_SESSION['user']['id'] == $value['user_id']) {
                                            if (isset($absen[$value['id']])) {
                                                $checked = $absen[$value['id']][0]['status'];
                                            }
                                            if ($checked == 'hadir') {
                                                $isHadir = true;
                                            }
                                        }
                                    }
                                    if ($isHadir) {
                                ?>
                                    <div class="alert alert-success" role="alert">
                                        Status Absensi Anda : Hadir
                                    </div>
                                <?php } else { 
                                    if (empty($checked)) {
                                        $color = 'info';
                                        $status_text = 'Belum dilakukan absensi';
                                    } else {
                                        $color = 'warning';
                                        $status_text = 'Tidak Hadir';
                                    }
                                ?>
                                    <div class="alert alert-<?= $color ?>" role="alert">
                                        Status Absensi Anda : <?= $status_text ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                            <?php 
                                if (isset($_SESSION['alert_anggota_ekstrakulikuler'])) {
                                    $alert = $_SESSION['alert_anggota_ekstrakulikuler'];
                            ?>
                                    <div class="alert alert-<?= $alert['icon'] ?>" role="alert">
                                        <?= $alert['message'] ?>
                                    </div>
                            <?php
                                    unset($_SESSION['alert_anggota_ekstrakulikuler']);
                                }
                            ?>
                            <div class="box">
                                <div class="box-header with-border">
                                <div class="col-12 d-flex justify-content-between">
                                    <h3 class="box-title">List Anggota Ekstrakulikuler <?= $ekstra['nama_ekstra'] ?></h3>
                                        <?php if ($_SESSION['user']['role'] != "USER") { ?>
                                            <button type="submit" class="btn btn-sm btn-primary"> Simpan</button>
                                        <?php } ?>
                                </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                    <table id="example123" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nama Anggota</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($data->num_rows > 0) { ?>
                                                <?php
                                                    while ($value = $data->fetch_assoc()) {
                                                        $checked = '';
                                                        if (isset($absen[$value['id']])) {
                                                            $checked = $absen[$value['id']][0]['status'];
                                                        }
                                                ?>
                                                    <tr>
                                                        <td><?= $value['nama_anggota'] ?></td>
                                                        <td>
                                                            <div class="c-inputs-stacked">
                                                                <input name="absen[<?= $value['id']?>]" type="radio" id="hadir_<?= $value['id']?>" value="1" <?= $checked == 'hadir' ? 'checked' : '' ?> <?= $_SESSION['user']['role'] == 'USER' ? 'disabled':'' ?>>
                                                                <label for="hadir_<?= $value['id']?>" class="mr-30">Hadir</label>
                                                                <input name="absen[<?= $value['id']?>]" type="radio" id="tidak_hadir_<?= $value['id']?>" value="0" <?= $checked == 'tidak_hadir' ? 'checked' : '' ?> <?= $_SESSION['user']['role'] == 'USER' ? 'disabled':'' ?>>
                                                                <label for="tidak_hadir_<?= $value['id']?>" class="mr-30">Tidak Hadir</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="4" class="text-center">Tidak ada data</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                                <th>Nama Anggota</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </section>
        </form>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Ekstrakulikuler</a>. SMK TI BALI GLOBAL DENPASAR.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
